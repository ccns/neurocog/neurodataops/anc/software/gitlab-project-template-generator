import gitlab
import logging
import fnmatch
from termcolor import colored


class GitlabApiWrapper:
    def __init__(self, gitlab_url, private_token):
        self.gl_instance = self.create_gitlab_instance(gitlab_url, private_token)

    def create_gitlab_instance(self, gitlab_url, private_token):
        try:
            gl_instance = gitlab.Gitlab(
                gitlab_url, private_token=private_token, ssl_verify=False
            )
            gl_instance.auth()
            return gl_instance
        except Exception as e:
            logging.error(f"Failed to create gitlab instance: {e}")
            raise

    @staticmethod
    def delete_project(gl_instance, project_id):
        try:
            response = gl_instance.http_request(
                "DELETE",
                path=f"/projects/{project_id}",
                query_parameters={"permanently_delete": "true"},
            )
            if response.status_code == 202:
                logging.info(
                    colored(f"Project with id {project_id} was deleted", "green")
                )
            else:
                logging.error(
                    colored(
                        f"Failed to delete project with id {project_id}. Status code: {response.status_code}",
                        "red",
                    )
                )
        except Exception as e:
            logging.error(
                colored(
                    f"An error occurred while trying to delete the project with id {project_id}: {str(e)}",
                    "red",
                )
            )

    @staticmethod
    def create_project(gl, project_name_with_namespace):
        try:
            namespace, project_name = project_name_with_namespace.rsplit("/", 1)
            namespace_id = gl.namespaces.get(namespace).id
            logging.info(
                colored(
                    f"New project {project_name} created with id {namespace_id}.",
                    "green",
                )
            )
            return gl.projects.create(
                {"name": project_name, "namespace_id": namespace_id}
            )
        except Exception as e:
            logging.error(colored(f"Failed to create project: {e}", "red"))
            raise

    @staticmethod
    def get_file_content(source_project, filename):
        branch = "main"

        try:
            file = source_project.files.get(file_path=filename, ref=branch)
        except gitlab.exceptions.GitlabGetError:
            logging.warning(
                colored(
                    f"File {filename} not found in source project, skipping.", "yellow"
                )
            )
            return None, None

        content = file.content

        return content, file

    @staticmethod
    # config_filenames is a list of filenames defined in the config file
    # source_filenames is a list of filenames copied from the source project (filenames from config + those matche by filepatterns)
    def get_filenames(source_project, config_filenames=None, filepatterns=None):
        all_files = []
        default_branch = "main"

        if not default_branch:
            logging.error(
                colored("Failed to get default branch from source project.", "red")
            )
            raise Exception("No default branch found in source project")

        try:
            all_files = [
                f["path"]
                for f in source_project.repository_tree(
                    ref=default_branch, recursive=True, get_all=True
                )
            ]
        except gitlab.exceptions.GitlabGetError as e:
            logging.error(
                colored(
                    f"Failed to get repository tree from source project: {e.error_message}",
                    "red",
                )
            )
            raise

        source_filenames = config_filenames or []

        if filepatterns:
            for pattern in filepatterns:
                pattern_files = [
                    filename
                    for filename in all_files
                    if fnmatch.fnmatch(filename, pattern)
                ]
                source_filenames.extend(pattern_files)

        if not source_filenames:
            logging.warning(
                colored(
                    "No files or patterns to copy were specified in config file. Copying all files from the source project.",
                    "yellow",
                )
            )
            source_filenames = all_files

        return source_filenames
