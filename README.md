# GitLab Project Template Generator

Creating projects from templates is a useful feature of GitLab. Unfortunately, the destination project is a full copy of the source project. In particular, the destination has the entire history, all issues and merge requests of the source. Sometimes this is not desired.

This project implements a configurable copying of a GitLab project. The copy is a new project without a history and can be further used as a clean template.

The following features are currently implemented:
- only files and issues are copied from the source project
- the destination project's history has only one commit
- files to copy can be specified by name or pattern
- files to copy can be renamed in the destination project
- issues to copy can be specified by label - issues with at least the specified labels will be copied
- if the destination project doesn't exist, it is created
- if the destination project exists, it is deleted and created

## Installation

Clone the project and execute from the root directory of the project.

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

The pre-commit hook were configured according to the [instructions in the coding guidelines](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/anc-data-steward-documentation/-/blob/main/Coding_Guidelines.md?ref_type=heads#4-styling-standards).

## Usage

```
template-generator.py [-h] -c CONFIG [-f] [-s]
```

where:
```
  -h, --help              show this help message and exit
  -c CONFIG, --config CONFIG
                        Path to yaml config file
  -f, --force           Force delete destination project if exists
  -s, --set-minimal-features
                        If used, the minimal project features are set for the destination project 
```

## Configuration

Create a config YAML file to specify what to copy to the destination project. The following configuration options are available. An [example of the files copied over can be found in our metatemplates](https://gitlab.com/ccns/neurocog/neurodataops/anc/templates/bids-basic/-/blob/main/CONTRIBUTING.md)

| Configuration parameter | Description |
| :---------------------- | :--------- |
| `source_gitlab_url` | Url of the source GitLab instance. |
| `dest_gitlab_url` | Url of the destination GitLab instance. |
| `source_private_token` | Token to the source GitLab instance. |
| `dest_private_token` | Token to the destination GitLab instance. |
| `source_project` | Source project name (with namespace) to copy from. |
| `dest_project` | Destination project name (with namespace). |
| `filenames` | List of file paths to copy from the source project. |
| `renames` | Optional file renaming. An object, where keys are file names in `filenames` list and values are new file names. |
| `filepatterns` | List of file patterns to copy from the source project. |
| `issue_labels` | Labels of the issues to copy from the source project. An issue must have at least these labels to be copied. |


# Configure project settings

The function `set_minimal_project_features` changes the following of the [projects that are created](https://docs.gitlab.com/ee/api/projects.html#create-a-project):

| **Feature**                        | **Action**                                                                 | **Setting**                                      |
|------------------------------------|-----------------------------------------------------------------------------|--------------------------------------------------|
| **Page Accessibility**             | Sets the project’s Pages feature to public access.                          | `project.pages_access_level = 'public'`          |
| **Wiki Access**                    | Disables the Wiki feature.                                                  | `project.wiki_access_level = 'disabled'`         |
| **Snippets Access**                | Disables the Snippets feature.                                              | `project.snippes_access_level = 'disabled'`      |
| **Container Registry**             | Disables the Container Registry.                                            | `project.container_registry_access_level = 'disabled'` |
| **Package Registry**               | Disables the Package Registry.                                              | `project.packages_enabled = False`               |
| **Model Registry**                 | Disables the Model Registry feature.                                        | `project.model_registry_access_level = 'disabled'` |
| **Environments**                   | Disables access to Environments.                                            | `project.environments_access_level = 'disabled'` |
| **Infrastructure**                 | Disables access to the Infrastructure feature.                              | `project.infrastructure_access_level = 'disabled'` |
| **Model Experiments**              | Disables access to Model Experiments.                                       | `project.model_experiments_access_level = 'disabled'` |
| **Monitoring**                     | Disables the Monitoring feature.                                            | `project.monitor_access_level = 'disabled'`      |
| **Analytics**                      | Disables the Analytics feature.                                             | `project.analytics_access_level = 'disabled'`    |
| **Releases**                       | Disables access to the Releases feature.                                    | `project.releases_access_level = 'disabled'`     |
| **Feature Flags**                  | Disables the Feature Flags functionality.                                   | `project.feature_flags_access_level = 'disabled'` |
| **Security and Compliance**        | Disables access to Security and Compliance features.                        | `project.security_and_compliance_access_level = 'disabled'` |
| **Auto DevOps**                    | Disables Auto DevOps for the project.                                       | `project.auto_devops_enabled = False`            |
| **Resolve Outdated Diff Discussions** | Enables automatic resolution of outdated diff discussions.                  | `project.resolve_outdated_diff_discussions = True` |
| **Squash Commit Merge**            | Sets the squash option for merge commits to be enabled by default.          | `project.squash_option = 'default_on'`           |

