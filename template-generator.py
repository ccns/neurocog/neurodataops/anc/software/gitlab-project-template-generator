import gitlab
import logging
import time
import yaml  # type: ignore
import urllib3
from termcolor import colored
import argparse
from GitlabApiWrapper import GitlabApiWrapper


# Disable SSL warnings for self-signed certificates instances
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)


class TemplateGenerator:
    def __init__(self, config_path):
        self.config = self.load_config(config_path)
        self.source_gl = GitlabApiWrapper(
            self.config["source_gitlab_url"], self.config["source_private_token"]
        )
        self.dest_gl = GitlabApiWrapper(
            self.config["dest_gitlab_url"], self.config["dest_private_token"]
        )

    def load_config(self, config_path):
        try:
            with open(config_path, "r") as config_file:
                config = yaml.safe_load(config_file)
                required_keys = [
                    "source_project",
                    "dest_project",
                    "source_gitlab_url",
                    "dest_gitlab_url",
                    "source_private_token",
                    "dest_private_token",
                ]
                if not all(key in config for key in required_keys):
                    raise ValueError(
                        "Config file must contain 'source_project', 'dest_project', 'source_gitlab_url', 'dest_gitlab_url', 'source_private_token', and 'dest_private_token' keys."
                    )
                config.setdefault("filenames", [])
                return config
        except Exception as e:
            logging.error(f"Failed to load config: {e}")
            raise

    def create_destination_project(self, force=False):
        try:
            project_name = self.config.get("dest_project")
            project = self.dest_gl.gl_instance.projects.get(project_name)
            if force:
                GitlabApiWrapper.delete_project(
                    self.dest_gl.gl_instance, project.get_id()
                )
                logging.info(
                    colored(
                        f"Destination project {project_name} found and deleted.",
                        "green",
                    )
                )
                time.sleep(
                    5
                )  # wait for 5 seconds for the project to be deleted since it has to be queued for deletion
                return GitlabApiWrapper.create_project(
                    self.dest_gl.gl_instance, project_name
                )
            else:
                logging.info(colored("Destination project will be created.", "green"))
                return GitlabApiWrapper.create_project(
                    self.dest_gl.gl_instance, project_name
                )
        except Exception:
            logging.error(
                colored(
                    f"Destination project {project_name} cannot be created. It might exist already. Use -f or --force to overwrite.",
                    "red",
                )
            )
            exit(1)

    def get_source_project(self):
        try:
            project_name = self.config.get("source_project")
            project = self.source_gl.gl_instance.projects.get(project_name)
        except gitlab.exceptions.GitlabGetError:
            logging.error(colored(f"Source project {project_name} not found.", "red"))
            raise
        return project

    def copy_issues(self, source_project, dest_project, issue_labels):
        try:
            source_issues = [
                issue
                for issue in source_project.issues.list(all=True)
                if set(issue.labels).intersection(issue_labels)
            ]

            for issue in source_issues:
                existing_issue = next(
                    (
                        i
                        for i in dest_project.issues.list(all=True)
                        if i.title == issue.title
                    ),
                    None,
                )
                if existing_issue:
                    existing_issue.title = issue.title
                    existing_issue.description = issue.description
                    existing_issue.labels = issue.labels
                    existing_issue.save()
                else:
                    dest_project.issues.create(
                        {
                            "title": issue.title,
                            "description": issue.description,
                            "labels": issue.labels,
                        }
                    )
        except Exception as e:
            logging.error(colored(f"Failed to copy issues: {e}", "red"))
            raise

    def copy_files(
        self, source_project, dest_project, config_filenames=None, filepatterns=None
    ):
        try:
            # Get the list of filenames to copy
            source_filenames = self.source_gl.get_filenames(
                source_project, config_filenames, filepatterns
            )
            actions = []

            for filename in source_filenames:
                content, file = GitlabApiWrapper.get_file_content(
                    source_project, filename
                )
                if content is not None:
                    # Rename the file if requested in config
                    file_path = file.file_path
                    if file_path in self.config["renames"]:
                        file_path = self.config["renames"][file_path]
                    action = {
                        "action": "create",
                        "file_path": file_path,
                        "content": content,
                        "encoding": "base64",
                    }
                    actions.append(action)

            if actions:
                dest_project.commits.create(
                    {
                        "branch": "main",
                        "commit_message": "Initial commit",
                        "actions": actions,
                    }
                )
        except Exception as e:
            logging.error(colored(f"Failed to copy files: {e}", "red"))
            raise

    def set_minimal_project_features(self, project):
        try:
            project.visibility = "public"
            # set project page accessibility to public
            project.pages_access_level = "public"
            # Disable wiki
            project.wiki_access_level = "disabled"
            # Disable snippets
            project.snippes_access_level = "disabled"
            # disbable container registry
            project.container_registry_access_level = "disabled"
            # disable packages
            project.packages_enabled = False
            # disable model registry
            project.model_registry_access_level = "disabled"
            # disable environments
            project.environments_access_level = "disabled"
            # disable infrastructure
            project.infrastructure_access_level = "disabled"
            # disable model experiments access
            project.model_experiments_access_level = "disabled"
            # disable monitoring
            project.monitor_access_level = "disabled"
            # disable analytics
            project.analytics_access_level = "disabled"
            # disable release access
            project.releases_access_level = "disabled"
            # disable feature flags
            project.feature_flags_access_level = "disabled"
            # disable security and compliance
            project.security_and_compliance_access_level = "disabled"
            # disable autodevops
            project.auto_devops_enabled = False
            # set to resolve outdated diff discussions
            project.resolve_outdated_diff_discussions = True
            # set squash commit merge
            project.squash_option = "default_on"
            project.save()
            logging.info(
                colored(f"Project {project.name} for GitLab settings updated.", "green")
            )
        except Exception as e:
            logging.error(
                colored(f"Failed to update project settings: {str(e)}", "red")
            )


def main():
    parser = argparse.ArgumentParser(description="Gitlab Template generator")
    parser.add_argument(
        "-c", "--config", help="Path to yaml config file", required=True
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Force delete destination project if exists",
    )
    parser.add_argument(
        "-s",
        "--set-minimal-features",
        action="store_true",
        help="Set minimal project features for the destination project",
    )
    args = parser.parse_args()

    templateGen = TemplateGenerator(args.config)

    source_project = templateGen.get_source_project()
    dest_project = templateGen.create_destination_project(args.force)

    templateGen.copy_files(
        source_project,
        dest_project,
        templateGen.config["filenames"],
        templateGen.config.get("filepatterns"),
    )
    templateGen.copy_issues(
        source_project, dest_project, templateGen.config.get("issue_labels")
    )

    if args.set_minimal_features:
        templateGen.set_minimal_project_features(dest_project)


if __name__ == "__main__":
    main()
